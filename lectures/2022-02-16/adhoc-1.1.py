
# ad hoc 1.1b
print(1+2+3+4+5)
print(5-4-3-2-1)
print(1/2/3/4/5)
print(1*2*3*4*5)

# ad hoc 1.1c
print("wenn fliegen hinter " + "fliegen " * 5 + "hinterher")
print("wenn fliegen hinter", "fliegen " * 5 + "hinterher")

print("wenn fliegen hinter" + " fliegen" * 5 + " hinterher")
print("wenn fliegen hinter" + " fliegen" * 5, "hinterher")

print("wenn fliegen hinter" , "aha" + " fliegen" * 5, "hinterher")