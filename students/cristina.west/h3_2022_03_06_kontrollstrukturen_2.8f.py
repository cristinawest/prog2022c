# h3 2.8f Schleife, Zahlen entgegennehmen: Bitte geben Sie Zahl 1 ein:
# h3, 2.8f) Zahl(nummeriert) eingeben können, wenn Summe > 10, soll Summe ausgegeben werden
summe = 0
zahl = 0
durchlauf = 0
while summe < 10:
    durchlauf = durchlauf + 1
    zahl = input('Bitte geben Sie Zahl ' + str(durchlauf) + ' ein: ')
    if zahl.isdigit():
        zahl = int(zahl)
        summe = summe + zahl
    if summe > 10:
        print('Summe:', summe)