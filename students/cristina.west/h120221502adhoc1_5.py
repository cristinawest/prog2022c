from turtle import *
size=numinput("pen","breite in px?")
konturfarbe=textinput("Dreieck","Konturfarbe?")
laenge=numinput("Dreieck","Seitenlänge?")
pensize(size); pencolor(konturfarbe); fillcolor("yellow"); begin_fill(); lt(150); fd(laenge); lt(120); fd(laenge); lt(120); fd(laenge); end_fill()
pensize(size); pencolor(konturfarbe); fillcolor("cyan"); begin_fill(); rt(120); fd(laenge/2); lt(120); fd(laenge/2); lt(120); fd(laenge/2); end_fill()
pensize(size); pencolor(konturfarbe); fillcolor("lightgreen"); begin_fill(); rt(120); fd(laenge/4); lt(120); fd(laenge/4); lt(120); fd(laenge/4); end_fill()