# adhoc 2.2b zweite variante
from turtle import*
def dreieck(seintenlaenge, fuellfarbe):
    fillcolor(fuellfarbe)
    begin_fill()
    fd(seintenlaenge)
    lt(120)
    fd(seintenlaenge)
    lt(120)
    fd(seintenlaenge)
    end_fill()

rt(90)
pencolor("red")
dreieck(100, "cyan")
dreieck(200, "yellow")
dreieck(50, "lime")
exitonclick()
