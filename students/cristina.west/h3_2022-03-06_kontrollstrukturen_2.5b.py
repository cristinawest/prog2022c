 #uebungsblatt kontrollstrukturen 2b) 4 Unterscheidungen der Altersangabe
def legal_status():
    print('Bitte Alter eingeben.')
    alter = int(input())
    if alter >=0 and alter <= 6:
        print('Mit', alter, 'ist man geschäftsunfahig.')
    elif alter >=7 and alter <=14:
        print('Mit', alter,'ist man unmündig.')
    elif alter >=14 and alter <18:
        print('Mit', alter,'ist man minderjährig.')
    elif alter <0:
        print('Ist man ungeboren.')
    else:
        print('Mit', alter, 'ist man volljährig.')