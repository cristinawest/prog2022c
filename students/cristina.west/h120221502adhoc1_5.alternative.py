#reihenfolge dreiecke beachte pfeilposition(south) und farbe(lightgreen) von uebung 1.5 ppp erste schritte in python prog
from turtle import *
size=numinput("pen","breite in px?") #5
konturfarbe=textinput("Dreieck","Konturfarbe?") #red
laenge=numinput("Dreieck","Seitenlänge?") #200
pensize(size); pencolor(konturfarbe); fillcolor("yellow"); begin_fill(); lt(150); fd(laenge); lt(120); fd(laenge); lt(120); fd(laenge); end_fill() #wie lang darf, sollte eine Zeile maximal sein?
laenge=laenge/2
pensize(size); pencolor(konturfarbe); fillcolor("cyan"); begin_fill(); rt(120); fd(laenge); lt(120); fd(laenge); lt(120); fd(laenge); end_fill()
laenge=laenge/2
pensize(size); pencolor(konturfarbe); fillcolor("lightgreen"); begin_fill(); rt(120); fd(laenge); lt(120); fd(laenge); lt(120); fd(laenge); end_fill()
exitonclick() #wird dieser befehl verlangt an pruefung?