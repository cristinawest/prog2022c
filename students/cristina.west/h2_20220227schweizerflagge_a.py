#Aussehen (400x400 Pixel)
 
#Bemerkung:Beim weissen Kreuz gibt es keine Vorgaben bzgl. Grösse,
#es muss aber symmetrisch sein und den gleichen Abstand zu allen Seiten der Flagge aufweisen
#Dokumentieren Sie Ihr Programm mit Kommentarzeilen.

#b) Erweitern Sie das Programm um eine Eingabemöglichkeit (über Eingabefenster)
#für die Seitenlänge der Schweizer Flagge!
#Tipp: Achten Sie bei den Seiten- bzw. Formberechnungen darauf,
#dass die turtle-Anweisungen mit ganzen Zahlen parametrisiert werden.
#Hinweis: mit int() können Kommazahlen in ganze Zahlen konvertiert werden.

#Dokumentieren Sie Ihr Programm mit Kommentarzeilen.
from turtle import*
ht() #turtle ist nicht sichtber
def quadrat(seitenlänge): #zuerst fläche 400x400 in rot für farbe zeichnen
    speed(10)
    fillcolor("red")
    pencolor("red")
    begin_fill()
    fd(seitenlänge)
    lt(90)
    fd(seitenlänge)
    lt(90)
    fd(seitenlänge)
    lt(90)
    fd(seitenlänge)
    end_fill()
    
quadrat(400)

#start weisses kreuz, position berechnen(400:5=80)
#-> start kreuz fd(160) rt(90) fd(80)

def kreuz(umfangsabschnitt):
    lt(90)
    fd(160)
    lt(90)
    fd(80)
    rt(90)
    speed(10)
    fillcolor("white")
    pencolor("white")
    begin_fill()
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    rt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    rt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    rt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    lt(90)
    fd(umfangsabschnitt)
    rt(90)
    fd(umfangsabschnitt)
    end_fill()

kreuz(80)
    
exitonclick()