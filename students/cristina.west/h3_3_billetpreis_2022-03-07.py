# h3 3) Billetpreis
def berechne_billetpreis():
    alter_in_jahren = int(input('Alter eingeben: '))   
    entfernung_in_kilometer = int(input('Entfernung eingeben: '))
    grundpreis_in_chf = 2
    kilometer_preis_in_chf = 0.25   
    summe = entfernung_in_kilometer * kilometer_preis_in_chf + grundpreis_in_chf
    if alter_in_jahren < 16:
        summe = summe * 0.5
    if alter_in_jahren < 6:
        summe = 0
    print('Ihr Billetpreis beträgt:' , summe)