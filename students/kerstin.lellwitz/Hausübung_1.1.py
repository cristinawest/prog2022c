#1.1.a

datensatz = ("Name\nVorname\nStrasse\nHausnummer\nPLZ\nWohnort") # Neue Zeile = \n
print(datensatz)

#Ich hätte es auch genau wie in der Übung verlangt, mit Komma (Name, Vorname, etc.) machen können.
#Dann hätte es folgender Code sein können:

datensatz = "Name, " "Vorname, " "Strasse, " "Hausnummer, " "PLZ, " "Wohnort"
print(datensatz)


#1.1.d

print(''.join(reversed('Hello'))) # ollaH

text = "Hello"
print(text[::-1])

#in unseren Unterlagen konnte ich nicht finden, wie wir etwas rückwärts schreiben können. Ich bin auf den
#Befehl reversed gestoßen, konnte ihn aber nicht entsprechend umsetzen. Dann habe ich angefangen im Netz zu
#recherhieren, und bin auf diese beiden Möglichkeiten gestoßen. Allerdings verstehe ich noch nicht, was bei
#der 1. Möglichkeit mit .join gemeint ist und was bei der 2. Möglichkeit ::-1 bedeutet.